package com.example.gestion_stock.repository;

import com.example.gestion_stock.domain.RolesDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RolesRepository extends JpaRepository<RolesDomain, Long> {
    @Query(value = "select * from roles where nom in (:names)", nativeQuery = true)
    List<RolesDomain> findByListName(@Param("names") List<String> names);
}
