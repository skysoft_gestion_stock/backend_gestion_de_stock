package com.example.gestion_stock.repository;


import com.example.gestion_stock.domain.LogDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface LogsRepository extends JpaRepository<LogDomain, Long>, JpaSpecificationExecutor<LogDomain> {
}
