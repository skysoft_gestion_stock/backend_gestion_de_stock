package com.example.gestion_stock.repository;

import com.example.gestion_stock.domain.RolesDomain;
import com.example.gestion_stock.domain.UtilisateurDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UtilisateurRepository extends JpaRepository<UtilisateurDomain, Long> {

    Optional<UtilisateurDomain> findByLogin(String login);
    Optional<UtilisateurDomain> findByEmail(String email);

    Optional<UtilisateurDomain> findOneWithAuthoritiesByLogin(String login);

    @Query(value = "SELECT u.roles from UtilisateurDomain as u where u.login = :login")
    List<RolesDomain> findRolesByLogin(@Param("login") String login);
}
