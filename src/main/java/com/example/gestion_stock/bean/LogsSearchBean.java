package com.example.gestion_stock.bean;

import com.example.gestion_stock.domain.enumeration.SourceLogs;
import com.example.gestion_stock.domain.enumeration.TypeLogs;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LogsSearchBean {
    private SourceLogs sourceLogs;
    private TypeLogs typeLogs;
    private String agent;
    private String dateDebutLogs;
    private String dateFinLogs;
    private boolean order;
    private int resultMax;
}
