package com.example.gestion_stock.service.criteria;

import com.example.gestion_stock.domain.enumeration.SourceLogs;
import com.example.gestion_stock.domain.enumeration.TypeLogs;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class LogsCriteria {

    private SourceLogs sourceLogs;
    private TypeLogs typeLogs;
    private String agent;
    private String dateDebutLogs;
    private String dateFinLogs;
    private boolean order;

}
