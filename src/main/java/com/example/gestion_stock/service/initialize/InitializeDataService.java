package com.example.gestion_stock.service.initialize;

import com.example.gestion_stock.domain.RolesDomain;
import com.example.gestion_stock.domain.UtilisateurDomain;
import com.example.gestion_stock.repository.RolesRepository;
import com.example.gestion_stock.repository.UtilisateurRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class InitializeDataService {


    private final UtilisateurRepository utilisateurRepository;
    private final RolesRepository rolesRepository;
    private final PasswordEncoder passwordEncoder;

    @PostConstruct
    public void init() {
        saveRole();
        saveUtilisateur();
    }


    private void saveRole() {
        List<String> rolesListName = new ArrayList<>(Collections.singletonList("ROLE_USER"));
        rolesListName.add("ROLE_ADMIN");
        for (String name : rolesListName) {
            if (rolesRepository.findByListName(Collections.singletonList(name)).isEmpty()) {
                RolesDomain role = new RolesDomain();
                role.setNom(name);
                rolesRepository.save(role);
            }
        }
    }


    private void saveUtilisateur() {
        UtilisateurDomain utilisateur = new UtilisateurDomain();
        if (utilisateurRepository.findByLogin("skysoft").isEmpty()) {
            utilisateur.setLogin("skysoft");
            utilisateur.setTelephone("675905795");
            utilisateur.setEmail("admin@skysoft.cm");
            setOtherInformationOfUser(utilisateur);
        } else {
            UtilisateurDomain utilisateur1 = utilisateurRepository.findByLogin("skysoft").get();
            setOtherInformationOfUser(utilisateur1);
        }


        if (utilisateurRepository.findByLogin("brice").isEmpty()) {
            utilisateur = new UtilisateurDomain();
            utilisateur.setLogin("brice");
            utilisateur.setTelephone("658748789");
            utilisateur.setEmail("brice@skysoft.cm");
            setOtherInformationOfUser(utilisateur);
        } else {
            UtilisateurDomain utilisateur1 = utilisateurRepository.findByLogin("brice").get();
            setOtherInformationOfUser(utilisateur1);
        }

        if (utilisateurRepository.findByLogin("rodrigue").isEmpty()) {
            utilisateur = new UtilisateurDomain();
            utilisateur.setLogin("serge");
            utilisateur.setTelephone("651618412");
            utilisateur.setEmail("rodrigue@skysoft.cm");
            setOtherInformationOfUser(utilisateur);
        } else {
            UtilisateurDomain utilisateur1 = utilisateurRepository.findByLogin("rodrigue").get();
            setOtherInformationOfUser(utilisateur1);
        }

        if (utilisateurRepository.findByLogin("philippe").isEmpty()) {
            utilisateur = new UtilisateurDomain();
            utilisateur.setLogin("philippe");
            utilisateur.setTelephone("690289055");
            utilisateur.setEmail("philippe@skysoft.cm");
            setOtherInformationOfUser(utilisateur);
        } else {
            UtilisateurDomain utilisateur1 = utilisateurRepository.findByLogin("philippe").get();
            setOtherInformationOfUser(utilisateur1);
        }

        if (utilisateurRepository.findByLogin("dessalli").isEmpty()) {
            utilisateur = new UtilisateurDomain();
            utilisateur.setLogin("dessalli");
            utilisateur.setTelephone("696925187");
            utilisateur.setEmail("dessalli@skysoft.cm");
            setOtherInformationOfUser(utilisateur);
        } else {
            UtilisateurDomain utilisateur1 = utilisateurRepository.findByLogin("dessalli").get();
            setOtherInformationOfUser(utilisateur1);
        }


    }

    private void setOtherInformationOfUser(UtilisateurDomain utilisateur) {
        if (utilisateurRepository.findRolesByLogin(utilisateur.getLogin()).isEmpty()) {
//            utilisateur.setAddress("Douala");
            utilisateur.setPassword(passwordEncoder.encode("12345"));
            utilisateur.setActiver(true);
            List<String> rolesListName = new ArrayList<>(Collections.singletonList("ROLE_USER"));
            rolesListName.add("ROLE_ADMIN");
            utilisateur.setRoles(rolesRepository.findByListName(rolesListName));
            utilisateurRepository.save(utilisateur);
        }
    }


}
