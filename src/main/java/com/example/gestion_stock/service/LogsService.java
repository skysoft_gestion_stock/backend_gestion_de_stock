package com.example.gestion_stock.service;


import com.example.gestion_stock.bean.LogsSearchBean;
import com.example.gestion_stock.dto.LogsDto;
import com.example.gestion_stock.exception.EntityNotFoundException;
import com.example.gestion_stock.exception.InvalidEntityException;
import com.example.gestion_stock.exception.InvalidIdException;

import java.util.List;

public interface LogsService {

    LogsDto saveLogs(LogsDto logsDto, String language) throws InvalidEntityException;

    List<LogsDto> findLogsByCriteria(LogsSearchBean searchBean);

    LogsDto findLogsById(Long logsId, String language) throws InvalidIdException, EntityNotFoundException;

}
