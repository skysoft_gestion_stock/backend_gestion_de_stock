package com.example.gestion_stock.service.impl;

import com.example.gestion_stock.domain.UtilisateurDomain;
import com.example.gestion_stock.exception.BadCredendialException;
import com.example.gestion_stock.mapper.UtilisateurMapper;
import com.example.gestion_stock.repository.UtilisateurRepository;
import com.example.gestion_stock.service.UtilisateurService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class UtilisateurServiceImpl implements UtilisateurService {
    private final UtilisateurRepository utilisateurRepository;
    private final UtilisateurMapper utilisateurMapper;
    @Override
    public UtilisateurDomain findByLogin(String login, String language) {
        return utilisateurRepository.findByLogin(login).orElseThrow(() ->
                new BadCredendialException("L'utilisateur avec le login = " + login + " n'a pas été trouvé dans le système")
        );
    }
}
