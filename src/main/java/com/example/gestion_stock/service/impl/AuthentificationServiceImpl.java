package com.example.gestion_stock.service.impl;


import com.example.gestion_stock.bean.AuthenticationRequestBean;
import com.example.gestion_stock.bean.AuthenticationResponseBean;
import com.example.gestion_stock.domain.TokensDomain;
import com.example.gestion_stock.domain.UtilisateurDomain;
import com.example.gestion_stock.exception.InvalidOperationException;
import com.example.gestion_stock.jwt.JwtUtil;
import com.example.gestion_stock.mapper.UtilisateurMapper;
import com.example.gestion_stock.repository.TokensRepository;
import com.example.gestion_stock.service.AuthentificationService;
import com.example.gestion_stock.service.UtilisateurService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class AuthentificationServiceImpl implements AuthentificationService {

    private final UtilisateurService utilisateurService;
    private final UtilisateurMapper utilisateurMapper;
    private final JwtUtil jwtUtil;
    private final TokensRepository tokensRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public AuthenticationResponseBean authenticateByLoginAndPassword(AuthenticationRequestBean request) {
        UtilisateurDomain userDto = utilisateurService.findByLogin(request.getLogin(), "fr");

        boolean response = comparePasswordUserForAuthenticate(request.getPassword(), userDto.getPassword());

        validFieldAuthenticate(response, userDto);

        String jwtToken = jwtUtil.generateToken(userDto);
        revokeAllUserToken(userDto);
        saveUserToken(userDto, jwtToken);
        return AuthenticationResponseBean.builder()
                .accessToken(jwtToken)
                .message("Vous êtes maintenant connecté à la plateforme").build();
    }

    public boolean comparePasswordUserForAuthenticate(String rawPassword, String passwordEncrypt) {
        return passwordEncoder.matches(rawPassword, passwordEncrypt);
    }

    private void validFieldAuthenticate(boolean response, UtilisateurDomain userDto) {
        if (!response) {
            throw new InvalidOperationException("Nom d'utililisateur ou mot de passe incorrect");
        }

        if (!userDto.isActiver()) {
            throw new InvalidOperationException("Votre compte n'est pas actif");
        }
    }

    private void saveUserToken(UtilisateurDomain user, String jwtToken) {
        var token = TokensDomain.builder()
                .utilisateur(user)
                .token(jwtToken)
                .expirer(false)
                .revoquer(false)
                .build();
        tokensRepository.save(token);
    }

    private void revokeAllUserToken(UtilisateurDomain user) {
        var validUserToken = tokensRepository.findAllValidTokenByUser(user.getId());
        if (validUserToken.isEmpty())
            return;
        validUserToken.forEach(token -> {
            token.setExpirer(true);
            token.setRevoquer(true);
        });
        tokensRepository.saveAll(validUserToken);
    }
}
