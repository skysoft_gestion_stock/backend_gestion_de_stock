package com.example.gestion_stock.service;


import com.example.gestion_stock.domain.UtilisateurDomain;

public interface UtilisateurService {
    UtilisateurDomain findByLogin(String login, String language);
}
