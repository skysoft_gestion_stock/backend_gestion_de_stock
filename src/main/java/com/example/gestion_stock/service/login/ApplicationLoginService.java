package com.example.gestion_stock.service.login;

import com.example.gestion_stock.domain.UtilisateurDomain;
import com.example.gestion_stock.repository.UtilisateurRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.openmbean.InvalidOpenTypeException;
import java.util.List;
import java.util.stream.Collectors;
@Service
@Slf4j
@RequiredArgsConstructor
public class ApplicationLoginService implements UserDetailsService {

    private final UtilisateurRepository utilisateurRepository;
    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login)  {
        log.debug("loadUserByUsername {}", login);

        return utilisateurRepository.findOneWithAuthoritiesByLogin(login)
                .map(user -> createSpringSecurityUser(login, user))
                .orElseThrow(() -> new UsernameNotFoundException("Nom d'utilisateur ou mot de passe incorrect"));
    }
    private org.springframework.security.core.userdetails.User createSpringSecurityUser(String login, UtilisateurDomain utilisateurDomain) {
        if (!utilisateurDomain.isActiver()) {
            throw new InvalidOpenTypeException("Le compte de l'utilisateur " + login + " n'est pas activé");
        }
        List<GrantedAuthority> grantedAuthorities = utilisateurDomain.getRoles().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getNom()))
                .collect(Collectors.toList());
        return new org.springframework.security.core.userdetails.User(utilisateurDomain.getLogin(),
                utilisateurDomain.getPassword(),
                grantedAuthorities);
    }
}
