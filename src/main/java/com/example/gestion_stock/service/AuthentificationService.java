package com.example.gestion_stock.service;


import com.example.gestion_stock.bean.AuthenticationRequestBean;
import com.example.gestion_stock.bean.AuthenticationResponseBean;

public interface AuthentificationService {

    AuthenticationResponseBean authenticateByLoginAndPassword(AuthenticationRequestBean request);
}
