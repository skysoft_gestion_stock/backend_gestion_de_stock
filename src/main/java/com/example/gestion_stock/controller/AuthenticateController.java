package com.example.gestion_stock.controller;

import com.example.gestion_stock.bean.AuthenticationRequestBean;
import com.example.gestion_stock.bean.AuthenticationResponseBean;
import com.example.gestion_stock.bean.ChangePasswordBean;
import com.example.gestion_stock.controller.api.AuthenticateApi;
import com.example.gestion_stock.dto.UtilisateursDto;
import com.example.gestion_stock.service.AuthentificationService;
import com.example.gestion_stock.service.logout.LogoutService;
import com.example.gestion_stock.util.MessageNotification;
import com.example.gestion_stock.util.ServiceUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequiredArgsConstructor
public class AuthenticateController implements AuthenticateApi {

    private final AuthentificationService authentificationService;
    private final LogoutService logoutService;
    private final ServiceUtil serviceUtil;

    @Override
    public AuthenticationResponseBean authenticateByLoginAndPassword(AuthenticationRequestBean request) {
        return authentificationService.authenticateByLoginAndPassword(request);
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        logoutService.logout(request, response, authentication);
    }

    @Override
    public MessageNotification changePassword(ChangePasswordBean bean, String language) {
        return null;
    }

    @Override
    public UtilisateursDto currentUser() {
        return null;
    }


}
