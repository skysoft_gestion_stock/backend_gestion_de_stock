package com.example.gestion_stock.controller;

import com.example.gestion_stock.bean.UserSaveBean;
import com.example.gestion_stock.controller.api.UtilisateurApi;
import com.example.gestion_stock.dto.UtilisateursDto;
import com.example.gestion_stock.util.MessageNotification;

import java.util.List;
public class UtilisateurController implements UtilisateurApi {
    @Override
    public MessageNotification save(UserSaveBean bean, String language) {
        return null;
    }

    @Override
    public List<UtilisateursDto> findAll(String login, String nom, String telephone, String type, String email, boolean classement, String typeClassement, Integer nombreDeResultat) {
        return null;
    }
}
