package com.example.gestion_stock.controller.api;

import com.example.gestion_stock.bean.UserSaveBean;
import com.example.gestion_stock.dto.UtilisateursDto;
import com.example.gestion_stock.util.Constants;
import com.example.gestion_stock.util.MessageNotification;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Api("utilisateur-api")
public interface UtilisateurApi {

    @ApiOperation(value = "Créer un nouvel utilisateur",
            notes = "Cette méthode permet de créer un nouvel utilisateur dans le système", response = MessageNotification.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "L'utilisateur a été créé avec succès"),
            @ApiResponse(code = 400, message = "L'objet n'est pas valide"),
            @ApiResponse(code = 403, message = "L'opération n'est pas valide")
    })
    @PostMapping(value = Constants.APP_API +"/users/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    MessageNotification save(@RequestBody UserSaveBean bean, @RequestParam(value = "language", required = false) String language);





    @ApiOperation(value = "Lister les utilisateurs du système",
            notes = "Cette méthode permet de lister les utilisateurs du système", responseContainer = "List<UtilisateursDto>")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Liste vide ou une liste d'utilisateur retournée")
    })
    @GetMapping(value = Constants.APP_API +"/users/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    List<UtilisateursDto> findAll(@RequestParam(value = "login", required = false) String login,
                                  @RequestParam(value = "nom", required = false) String nom,
                                  @RequestParam(value = "telephone", required = false) String telephone,
                                  @RequestParam(value = "type", required = false) String type,
                                  @RequestParam(value = "email", required = false) String email,
                                  @RequestParam(value = "classement", required = false) boolean classement,
                                  @RequestParam(value = "typeClassement", required = false) String typeClassement,
                                  @RequestParam(value = "nombreDeResultat", required = false) Integer nombreDeResultat);
}
