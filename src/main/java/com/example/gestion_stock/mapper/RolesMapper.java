package com.example.gestion_stock.mapper;

import com.example.gestion_stock.domain.RolesDomain;
import com.example.gestion_stock.dto.RolesDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RolesMapper extends EntityMapper<RolesDto, RolesDomain> {
}
