package com.example.gestion_stock.mapper;

import com.example.gestion_stock.domain.LogDomain;
import com.example.gestion_stock.dto.LogsDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LogsMapper extends EntityMapper<LogsDto, LogDomain> {
}
