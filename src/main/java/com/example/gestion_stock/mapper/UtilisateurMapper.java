package com.example.gestion_stock.mapper;

import com.example.gestion_stock.domain.UtilisateurDomain;
import com.example.gestion_stock.dto.UtilisateursDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UtilisateurMapper extends EntityMapper<UtilisateursDto, UtilisateurDomain> {
}
