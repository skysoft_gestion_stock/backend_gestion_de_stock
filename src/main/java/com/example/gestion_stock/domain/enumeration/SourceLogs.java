package com.example.gestion_stock.domain.enumeration;

public enum SourceLogs {
    GESTION_CONTACT,
    GESTION_AGENCE,
    GESTION_UTILISATEUR,
}
