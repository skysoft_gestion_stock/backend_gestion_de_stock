package com.example.gestion_stock.domain.enumeration;

public enum TypePayementSms {
    PAYEMENT_PREPAID,
    PAYEMENT_POSTPAID
}
