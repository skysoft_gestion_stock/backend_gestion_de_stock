package com.example.gestion_stock.domain.enumeration;

public enum TypeLogs {
    CONNEXIONSYSTEME,
    DECONNEXIONSYSTEME,
    ENREGISTREMENT,
    UPDATE,
    DELETE,
    ENABLE,
    DISABLE,
}
