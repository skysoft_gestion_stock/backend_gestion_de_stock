package com.example.gestion_stock.domain.enumeration;

public enum TypeEnvoiSms {
    WEB,
    MOBILE
}
