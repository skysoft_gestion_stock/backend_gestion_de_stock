package com.example.gestion_stock.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "utilisateur")
public class UtilisateurDomain implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreatedDate
    @Column(name = "date_creation", nullable = false, updatable = false)
    private LocalDateTime dateCreation;

    @LastModifiedDate
    @Column(name = "derniere_date_modification", nullable = false)
    private LocalDateTime derniereDateModification;
    @Column(name = "nom")
    private String nom;

    @Column(name = "login", nullable = false)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "telephone", nullable = false)
    private String telephone;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "activer", columnDefinition="tinyint(1) default 1", nullable = false)
    private boolean activer;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<RolesDomain> roles;

    @PrePersist
    void persist() {
        dateCreation = LocalDateTime.now();
        derniereDateModification = LocalDateTime.now();
    }

    @PreUpdate
    void update() {
        derniereDateModification = LocalDateTime.now();
    }
}
