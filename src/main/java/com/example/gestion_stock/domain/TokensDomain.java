package com.example.gestion_stock.domain;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;



@Getter
@Setter
@Builder
@Entity
@Table(name = "tokens")
@NoArgsConstructor
@AllArgsConstructor
public class TokensDomain  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreatedDate
    @Column(name = "date_creation", nullable = false, updatable = false)
    private LocalDateTime dateCreation;

    @LastModifiedDate
    @Column(name = "derniere_date_modification", nullable = false)
    private LocalDateTime derniereDateModification;
    @Column(name = "token")
    private String token;

    @Column(name = "expirer", columnDefinition = "tinyint(1) default 0", nullable = false)
    private boolean expirer;

    @Column(name = "revoquer", columnDefinition = "tinyint(1) default 0", nullable = false)
    private boolean revoquer;

    @ManyToOne
    private UtilisateurDomain utilisateur;

    @PrePersist
    void persist() {
        dateCreation = LocalDateTime.now();
        derniereDateModification = LocalDateTime.now();
    }

    @PreUpdate
    void update() {
        derniereDateModification = LocalDateTime.now();
    }
}
