package com.example.gestion_stock.util;

import com.example.gestion_stock.domain.UtilisateurDomain;
import com.example.gestion_stock.domain.enumeration.SourceLogs;
import com.example.gestion_stock.domain.enumeration.TypeLogs;
import com.example.gestion_stock.dto.LogsDto;
import com.example.gestion_stock.dto.RolesDto;
import com.example.gestion_stock.dto.UtilisateursDto;
import com.example.gestion_stock.mapper.UtilisateurMapper;
import com.example.gestion_stock.repository.UtilisateurRepository;
import com.example.gestion_stock.service.LogsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ServiceUtil {
    private static String CHAR_NUMERIQUE = "0123456789";
    private static char sep1 = '-', sep2 = '/', sep3 = ';', prefixe1 = '+';
    private final LogsService logsService;
    private final UtilisateurRepository utilisateurRepository;
    private final UtilisateurMapper utilisateurMapper;

    public void addLogs(SourceLogs sourceLogs, TypeLogs typeLogs, String libelle, String language) {
        UtilisateursDto utilisateursDto = currentUser();
        LogsDto logsDto = LogsDto.builder()
                .sourceLogs(sourceLogs)
                .typeLogs(typeLogs)
                .message(libelle)
                .agent(utilisateursDto.getLogin())
                .build();
        logsService.saveLogs(logsDto, language);
    }

    public UtilisateursDto currentUser() {
        Optional<UtilisateurDomain> currentUser = utilisateurRepository.findByLogin(SecurityUtils.getCurrentUserLogin());
        return currentUser.map(utilisateurMapper::fromEntity).orElse(null);
    }

    public List<String> getCurrentUserRoles() {
        UtilisateursDto user = currentUser();
        List<String> roles = new ArrayList<>();
        for (RolesDto rolesDto : user.getRoles()) {
            roles.add(rolesDto.getNom());
        }
        return roles;
    }

    public Boolean isCurrentUserAdmin() {
        return getCurrentUserRoles().contains("ROLE_ADMIN");
    }
}
