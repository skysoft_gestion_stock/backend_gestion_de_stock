package com.example.gestion_stock.util;

public interface Constants {
    public static final String DEFAULT_LANGUAGE = "fr";
    public static final String SOURCE_LOGS = "sourceLogs";
    public static final String TYPE_LOGS = "typeLogs";
    public static final String DATE_LOGS = "dateoperation";
    public static final     String AGENT = "agent";  public static final String LOGS_ID = "id";
    String APP_API = "api/skysoft/v1";
}
