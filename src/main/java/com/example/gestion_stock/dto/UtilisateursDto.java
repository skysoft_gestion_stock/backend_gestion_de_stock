package com.example.gestion_stock.dto;


import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UtilisateursDto {

    private Long id;
    private String nom;
    private String login;
    private String password;
    private String email;
    private String telephone;
    private boolean activer;
    private List<RolesDto> roles;
}
